* [Tutorial de inicio a .NET Core 2](https://docs.microsoft.com/es-es/aspnet/core/tutorials/first-mvc-app)

# Requisitos 
* [.NET Core SDK](https://www.microsoft.com/net/download/windows/build))

* [Visual code](https://code.visualstudio.com/download/) - Opcional

## Proyecto ASP.NET Core Web App (Model-View-Controller) creado con .NET Core
Puedes crear uno con los siguientes comando en terminal 
```
$ dotnet new mvc    //Crear un nuevo proyecto 
$ dotnet build      //Compila el proyecto sólo la primera vez o al agregar nuevas dependencias 
$ dotnet run        //Corre el proyecto, cada vez que se modifique un archivo C# es necesario (para html, css, js no es necesario)
```
 
## Estructura del proyecto 
* [Controllers](https://docs.microsoft.com/es-es/aspnet/core/mvc/controllers/actions)
* Models
* [Views](https://docs.microsoft.com/es-es/aspnet/core/mvc/views/overview)
* wwwroot

# Controllers
Por fines de practicos y sólo por uso para frontend solamente sirven como punto de entrada para las peticiones y regresan una **vista** o pagina web.

* Los controladores son clases de c# .cs que deben llevar el prefijo Controller y extender de Controller.
* A cada método del controlador se le conoce como ación.
* Cada controlador necesita una carpeta Con el mismo nombre en **Views** y cada acción necesita un archivo con el mismo nombre y extención .cshtml (html con código c#)
* Los metodos del controlador serán del tipo IActionResult y y como retorno será una View().

# Models 
Por fines de frontend no seran necesairios, sólo se trabajará más con la carpeta _wwwroot_.

# Views
* Aquí van las vistas o archivos html que seran renderizados desde alguna acción de un controlador.
* La carpeta **Shared** contiene arvhicos _"parciales"_ que inician con guión bajo que son fragmento de html que se puede reutilizar en otras vistas.
* El arvchivo ../Views/_Layout.cshtml es un master layout donde se cargan otras vistas en el método **@RenderBody()**, esté layour contiene el _nav_ y el _body_ del html, sirve para no repetir este código en todos los layout.

# wwwroot
* Esta caperta contiene todos los archivos js, css e imagenes que es el directorio web que se carga en el navegador del cliente

# obj 
* Es la carpeta donde eestan los compilados del proyecto. 

# Startup.cs
* Es el arvhivo que contiene configuraciones como las de las rutas, es mas utiliciado por backend

# test1.csproj
* Archivo del proyecto 

## Todo lo demas son cosas raras de .net supongo, no necesarias por el momento